#pragma once
class Anel{
private:
	float p_inclinacao,p_rotacao, p_raiointerno, p_raioexterno;
	int p_textura;
public:
	Anel(float inclinacao,float rotacao, float raiointerno, float raioexterno, int textura);

	float getInclinacao() { return p_inclinacao; }
	float getRotacao() { return p_rotacao; }
	float getRaioInterno() { return p_raiointerno; }
	float getRaioExterno() { return p_raioexterno; }
	int getTextura() { return p_textura; }
};

