#pragma once

//raio
#define RAIO_SOL 20
#define RAIO_MERCURIO 3
#define RAIO_VENUS 5
#define RAIO_TERRA 3
#define RAIO_LUA 1
#define RAIO_MARTE 4.5
#define RAIO_JUPITER 12
#define RAIO_SATURNO 10
#define RAIO_URANO 8
#define RAIO_NEPTUNO 10
#define RAIO_PLUTAO 2

// velocidade de rota��o no proprio eixo 
#define ROT_SOL 2
#define ROT_MERCURIO 1
#define ROT_VENUS 2
#define ROT_TERRA 4
#define ROT_LUA -1
#define ROT_MARTE 2
#define ROT_JUPITER 6
#define ROT_SATURNO 7
#define ROT_URANO 9
#define ROT_NEPTUNO 11
#define ROT_PLUTAO 12

//distancia ao sol
#define DIST_SOL 0
#define DIST_MERCURIO 35
#define DIST_VENUS 70
#define DIST_TERRA 105
#define DIST_LUA 6
#define DIST_MARTE 140
#define DIST_JUPITER 175
#define DIST_SATURNO 210
#define DIST_URANO 245
#define DIST_NEPTUNO 280
#define DIST_PLUTAO 315

//velocidade de rota��o a volta do sol
#define ROT_S_MERCURIO 0.2
#define ROT_S_VENUS 0.3
#define ROT_S_TERRA 0.15
#define ROT_S_MARTE 0.22
#define ROT_S_JUPITER 0.1
#define ROT_S_SATURNO 0.25
#define ROT_S_URANO 0.3
#define ROT_S_NEPTUNO 0.21
#define ROT_S_PLUTAO 0.08

// aneis
#define INC_ANEL_SATURNO 25
#define ANEL_SATURNO_INT 12
#define ANEL_SATURNO_EXT 16
#define ANEL_SATURNO_ROT 10

#define INC_ANEL_URANO 80
#define ANEL_URANO_INT 11
#define ANEL_URANO_EXT 12
#define ANEL_URANO_ROT 10

//fontes de luz (placed f)
GLfloat light_ambient[] = { 0.001f, 0.001f, 0.001f, 1.0f };
GLfloat light_diffuse[] = { 1.0f, 1.0f, 1.0f, 1.0f };
GLfloat light_specular[] = { 1.0f, 1.0f, 1.0f, 1.0f };
GLfloat light_position[] = { 0.0f, 0.0f, 0.0f, 1.0f };
GLfloat light_position2[] = { 320.0f, 0.0f, 0.0f, 1.0f };

GLfloat mat_A[] = { 0.22f, 0.06f, 0.11f, 1.0f };
GLfloat mat_D[] = { 0.43f, 0.47f, 0.54f, 1.0f };
GLfloat mat_S[] = { 0.33f, 0.33f, 0.42f, 1.0f };
GLfloat mat_E[] = { 0.0f, 0.0f, 0.0f, 1.0f };
GLfloat mat_Shiny =11;

//outros
#define LARG_ORBITA 0.2
 