﻿#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <math.h>
#include <time.h>
#include <GL/glut.h>

#pragma warning(disable:4996)
#pragma comment(lib, "irrKlang.lib")
#include <irrKlang.h>
using namespace irrklang;

#include "Texturas.h"
#include "ConstantesPlanetas.h"
#include "Planeta.h"
#include "Anel.h"
#include "glm.h"

#ifndef M_PI
#define M_PI 3.1415926535897932384626433832795
#endif

#define RAD(x)          (M_PI*(x)/180)
#define GRAUS(x)        (180*(x)/M_PI)

#define DEBUG               1

#define DELAY_MOVIMENTO     20
#define RAIO_ROTACAO        20

extern "C" int read_JPEG_file(char*, char**, int*, int*, int*);

GLMmodel* pmodel = NULL;

/* VARIAVEIS GLOBAIS */

typedef struct {
	GLboolean   q, a, z, x, up, down, left, right;
}Teclas;

typedef struct {
	GLfloat    x, y, z;
}Pos;

typedef struct {
	Pos      eye, center, up;
	GLfloat  fov;
}Camera;

typedef struct {
	GLboolean   doubleBuffer;
	GLint       delayMovimento;
	Teclas      teclas;
	GLuint      menu_id;
	GLuint      selected;
	GLboolean   menuActivo;
	Camera      camera;
	GLboolean   debug;
	GLboolean   ortho;
	GLboolean rotateX;
	GLboolean rotateY;
	GLboolean play;
	ISoundEngine *SoundEngine;
	GLfloat sound;
}Estado;

typedef struct {
	GLint raio;
	GLdouble angulo_x;
	GLdouble angulo_y;
}Controlo;

typedef struct {
	GLfloat   theta[3];     // 0-Rotação em X; 1-Rotação  em Y; 2-Rotação  em Z
	GLuint textures_id[TEX_NUM];
	GLfloat scale;
	char* selecionado;
}Modelo;

Estado estado;
Modelo modelo;
Controlo controlo;

const char* MAIN_TRACK = "sounds/halo.wav";
Planeta mercurio(DIST_MERCURIO, ROT_S_MERCURIO, RAIO_MERCURIO, ROT_MERCURIO, TEX_MERCURIO, "Mercurio");
Planeta venus(DIST_VENUS, ROT_S_VENUS, RAIO_VENUS, ROT_VENUS, TEX_VENUS, "Venus");
Planeta terra(DIST_TERRA, ROT_S_TERRA, RAIO_TERRA, ROT_TERRA, TEX_TERRA, "Terra");
Planeta marte(DIST_MARTE, ROT_S_MARTE, RAIO_MARTE, ROT_MARTE, TEX_MARTE, "Marte");
Planeta jupiter(DIST_JUPITER, ROT_S_JUPITER, RAIO_JUPITER, ROT_JUPITER, TEX_JUPITER, "Jupiter");
Planeta saturno(DIST_SATURNO, ROT_S_SATURNO, RAIO_SATURNO, ROT_SATURNO, TEX_SATURNO, "Saturno");
Planeta urano(DIST_URANO, ROT_S_URANO, RAIO_URANO, ROT_URANO, TEX_URANO, "Urano");
Planeta neptuno(DIST_NEPTUNO, ROT_S_NEPTUNO, RAIO_NEPTUNO, ROT_NEPTUNO, TEX_NEPTUNO, "Neptuno");
Planeta plutao(DIST_PLUTAO, ROT_S_PLUTAO, RAIO_PLUTAO, ROT_PLUTAO, TEX_PLUTAO, "Plutao");

Planeta lua(DIST_LUA, ROT_LUA, RAIO_LUA, ROT_LUA, TEX_LUA, "Lua");

Anel anel_saturno(INC_ANEL_SATURNO, ANEL_SATURNO_ROT, ANEL_SATURNO_INT, ANEL_SATURNO_EXT, TEX_SATURNO_ANEL);
Anel anel_urano(INC_ANEL_URANO, ANEL_URANO_ROT, ANEL_URANO_INT, ANEL_URANO_EXT, TEX_URANO_ANEL);

void inicia_modelo()
{
	modelo.theta[0] = 0;
	modelo.theta[1] = 0;
	modelo.theta[2] = 0;
	modelo.scale = 1;

	modelo.selecionado = (char *) malloc(256);

	controlo.raio = 100;
	controlo.angulo_x = 0;
	controlo.angulo_y = 0;
}

void readTextura(char * filename, int index) {
	char* image;
	int w, h, bpp;
	glBindTexture(GL_TEXTURE_2D, modelo.textures_id[index]);
	if (read_JPEG_file(filename, &image, &w, &h, &bpp)) {
		gluBuild2DMipmaps(GL_TEXTURE_2D, 3, w, h, GL_RGB, GL_UNSIGNED_BYTE, image);
	}
}

void loadTexturas() {
	glGenTextures(TEX_NUM, modelo.textures_id);
	readTextura("textures/sunmap2.jpg", TEX_SOL);
	readTextura("textures/mercurymap.jpg", TEX_MERCURIO);
	readTextura("textures/venusmap.jpg", TEX_VENUS);
	readTextura("textures/earthmap.jpg", TEX_TERRA);
	readTextura("textures/moonmap.jpg", TEX_LUA);
	readTextura("textures/marsmap.jpg", TEX_MARTE);
	readTextura("textures/jupitermap.jpg", TEX_JUPITER);
	readTextura("textures/saturnmap.jpg", TEX_SATURNO);
	readTextura("textures/saturnring.jpg", TEX_SATURNO_ANEL);
	readTextura("textures/uranusmap.jpg", TEX_URANO);
	readTextura("textures/uranosring.jpg", TEX_URANO_ANEL);
	readTextura("textures/neptunemap.jpg", TEX_NEPTUNO);
	readTextura("textures/plutomap.jpg", TEX_PLUTAO);
	readTextura("textures/universe.jpg", TEX_UNIVERSO);
	//glTexEnvi(GL_TEXTURE_2D, GL_TEXTURE_ENV_MODE, GL_MODULATE);
}

/* Inicialização do ambiente OPENGL */
void Init(void)
{
	srand((unsigned)time(NULL));

	estado.debug = DEBUG;
	estado.menuActivo = GL_FALSE;
	estado.delayMovimento = DELAY_MOVIMENTO;
	estado.camera.eye.x = 40;
	estado.camera.eye.y = 340;
	estado.camera.eye.z = 70;
	estado.camera.center.x = 0;
	estado.camera.center.y = 0;
	estado.camera.center.z = 0;
	estado.camera.up.x = 0;
	estado.camera.up.y = 0;
	estado.camera.up.z = 1;
	estado.ortho = GL_FALSE;
	estado.camera.fov = 60;
	estado.play = true;
	estado.SoundEngine = createIrrKlangDevice();
	estado.sound = 0.2;
	estado.selected = 0;

	estado.teclas.a = estado.teclas.q = estado.teclas.z = estado.teclas.x = \
		estado.teclas.up = estado.teclas.down = estado.teclas.left = estado.teclas.right = GL_FALSE;

	inicia_modelo();
	loadTexturas();

	//glClearColor(0.0, 0.0, 0.0, 0.0);

	//enables aqui

	/*TRACKS*/
	estado.SoundEngine->play2D(MAIN_TRACK, GL_TRUE);
}

/**************************************
***  callbacks de janela/desenho    ***
**************************************/

// CALLBACK PARA REDIMENSIONAR JANELA
void setView(GLboolean Picking, int x, int y) {
	int vport[4];

	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	if (Picking) { // se est� no modo picking, l� viewport e define zona de picking
		glGetIntegerv(GL_VIEWPORT, vport);
		gluPickMatrix(x, vport[3] - y, 4, 4, vport); // zona sobre o rato (+/-)
	}
	gluPerspective(estado.camera.fov, (GLfloat)950 / 500, 1, 1000);
	glMatrixMode(GL_MODELVIEW);
}

void Reshape(int width, int height)
{
	glViewport(0, 0, (GLint)width, (GLint)height);
	setView(GL_FALSE, 0, 0);
}

// ... definicao das rotinas auxiliares de desenho ...

void drawmodel(void)
{
	glPushMatrix(); {

		if (!pmodel) {
			pmodel = glmReadOBJ("obj/alien.obj");
			if (!pmodel) exit(0);
			glmUnitize(pmodel);
			glmScale(pmodel, 1);
			glmFacetNormals(pmodel);
			//glmVertexNormals(pmodel, 90.0);
		}
	
		glmDraw(pmodel, GLM_SMOOTH);
	}glPopMatrix();
}

void bitmapCenterString(char *str, double x, double y)
{
	int i, n;

	n = (int)strlen(str);
	glRasterPos2d(x - glutBitmapLength(GLUT_BITMAP_HELVETICA_18, (const unsigned char *)str)*0.5, y);
	for (i = 0; i < n; i++)
		glutBitmapCharacter(GLUT_BITMAP_HELVETICA_18, (int)str[i]);
}

void desenhaEsfera(GLdouble escala, GLdouble rotacao, int textura) {
	//define escala da esfera
	escala /= modelo.scale;
	glScaled(escala, escala, escala);

	GLUquadricObj *qObj = gluNewQuadric();
	gluQuadricNormals(qObj, GLU_SMOOTH);
	gluQuadricTexture(qObj, GL_TRUE);

	glRotated(modelo.theta[0] * rotacao, 0, 0, 1);

	glEnable(GL_TEXTURE_2D);
	glBindTexture(GL_TEXTURE_2D, modelo.textures_id[textura]);
	gluSphere(qObj, 1, 20, 20);
	glDisable(GL_TEXTURE_2D);
}

void translacaoPlaneta(GLfloat distancia, GLfloat velocidade, Planeta &planeta) {
	GLfloat x_coord = distancia * cos(RAD(modelo.theta[0] * velocidade));
	GLfloat y_coord = distancia * sin(RAD(modelo.theta[0] * velocidade));
	glTranslated(x_coord, y_coord, 0);
	planeta.setCoords(x_coord, y_coord);
}

void orbita(GLfloat raio) {
	raio /= modelo.scale;
	GLUquadricObj *qObj = gluNewQuadric();
	gluQuadricNormals(qObj, GLU_SMOOTH);
	gluDisk(qObj, raio, raio + 0.2, 60, 60);
}

void desenhaSol() {
	glPushMatrix(); {
		glMaterialfv(GL_FRONT, GL_EMISSION, light_diffuse);
		desenhaEsfera(RAIO_SOL, ROT_SOL, TEX_SOL);
	}glPopMatrix();
}

void desenhaPlanetaComLuas(Planeta &planeta, Planeta &lua) {
	glPushMatrix(); {
		orbita(planeta.getDistancia());
		translacaoPlaneta(planeta.getDistancia(), planeta.getRotacaoSol(), planeta);

		glPushMatrix(); {
			desenhaEsfera(planeta.getRaio(), planeta.getRotacao(), planeta.getTextura());
		}glPopMatrix();

		glPushMatrix(); {
			translacaoPlaneta(lua.getDistancia(), lua.getRotacaoSol(), lua);
			desenhaEsfera(lua.getRaio(), lua.getRotacao(), lua.getTextura());
		}glPopMatrix();
	}glPopMatrix();
}

void desenhaPlanetaComAlien(Planeta &planeta) {
	glPushMatrix(); {
		orbita(planeta.getDistancia());
		translacaoPlaneta(planeta.getDistancia(), planeta.getRotacaoSol(), planeta);

		glPushMatrix(); {
			desenhaEsfera(planeta.getRaio(), planeta.getRotacao(), planeta.getTextura());
		}glPopMatrix();
	
		glPushMatrix(); {
			GLfloat x_coord = 5 * cos(RAD(modelo.theta[0] * -planeta.getRotacaoSol()));
			GLfloat y_coord = 5 * sin(RAD(modelo.theta[0] * -planeta.getRotacaoSol()));
			glTranslated(x_coord, y_coord, 0);
			glRotated(modelo.theta[0] * -planeta.getRotacaoSol(), 0, 0, 1);
			drawmodel();
		}glPopMatrix();
	}glPopMatrix();
}

void desenhaPlanetaGenerico(Planeta &planeta) {
	glPushMatrix(); {
		orbita(planeta.getDistancia());
		translacaoPlaneta(planeta.getDistancia(), planeta.getRotacaoSol(),planeta);
		glPushMatrix(); {
			desenhaEsfera(planeta.getRaio(), planeta.getRotacao(), planeta.getTextura());
		}glPopMatrix();
	}glPopMatrix();
}

//anel com glu disk
void desenhaAnel2(int textura, int raio_int, int raio_ext) {
	GLUquadricObj *qObj = gluNewQuadric();
	gluQuadricNormals(qObj, GLU_SMOOTH);
	gluQuadricTexture(qObj, GL_TRUE);
	glEnable(GL_TEXTURE_2D);
	glBindTexture(GL_TEXTURE_2D, modelo.textures_id[textura]);
	gluDisk(qObj, raio_int, raio_ext, 50, 50);
	glDisable(GL_TEXTURE_2D);
}

// anel com triangle strip
void desenhaAnel(int textura, int raio_int, int raio_ext) {
	glEnable(GL_TEXTURE_2D);
	glBindTexture(GL_TEXTURE_2D, modelo.textures_id[textura]);

	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

	glBegin(GL_TRIANGLE_STRIP);
	glColor4f(1.0f, 1.0f, 1.0f,0.85);
	double ratio = 360 / 40;
	double ang = 0;
	for (int i = 0; i < 160; i++) {
		ang += ratio;
		GLfloat x1 = raio_int*cos(RAD(ang));
		GLfloat y1 = raio_int*sin(RAD(ang));
		GLfloat x0 = raio_ext*cos(RAD(ang));
		GLfloat y0 = raio_ext*sin(RAD(ang));

		if (i % 2 == 0) {
			glTexCoord2f(0.0, 0.0);
			glVertex2f(x0, y0);
			glTexCoord2f(1.0, 0.0);
			glVertex2f(x1, y1);
		}
		else {
			glTexCoord2f(0.0, 0.1);
			glVertex2f(x0, y0);
			glTexCoord2f(1.0, 1.0);
			glVertex2f(x1, y1);
		}
	}
	glEnd();
	glDisable(GL_BLEND);
}

void desenhaPlanetaAnel(Planeta &planeta, Anel &anel) {
	glPushMatrix(); {
		orbita(planeta.getDistancia());
		translacaoPlaneta(planeta.getDistancia(), planeta.getRotacaoSol(), planeta);
		glPushMatrix(); {
			desenhaEsfera(planeta.getRaio(), planeta.getRotacao(), planeta.getTextura());
		}glPopMatrix();

		glPushMatrix(); {
			glRotated(anel.getInclinacao(), 0, 1, 0);
			glRotated(modelo.theta[0] * anel.getRotacao(), 0, 0, 1);
			desenhaAnel(anel.getTextura(), anel.getRaioInterno(), anel.getRaioExterno());
		}glPopMatrix();
	}glPopMatrix();
}

void desenhaUniverso(int textura, GLdouble tamanho) {
	glPushMatrix(); {
		glPushMatrix(); {
			glMaterialfv(GL_FRONT, GL_EMISSION, light_specular);
			desenhaEsfera(tamanho, 0, textura);
			glMaterialfv(GL_FRONT_AND_BACK, GL_EMISSION, light_ambient);
		}glPopMatrix();
	}glPopMatrix();
}


// Callback de desenho

void setCamera() {
	gluLookAt(estado.camera.eye.x, estado.camera.eye.y, estado.camera.eye.z, \
		estado.camera.center.x, estado.camera.center.y, estado.camera.center.z, \
		estado.camera.up.x, estado.camera.up.y, estado.camera.up.z);
}

void preparaCena3D() {
	glEnable(GL_COLOR_MATERIAL);
	glEnable(GL_POINT_SMOOTH);
	glEnable(GL_LINE_SMOOTH);
	glEnable(GL_POLYGON_SMOOTH);
	glEnable(GL_DEPTH_TEST);

	glEnable(GL_LIGHTING);
	glEnable(GL_DEPTH_TEST);
	glLightfv(GL_LIGHT0, GL_AMBIENT, light_ambient);
	glLightfv(GL_LIGHT0, GL_DIFFUSE, light_diffuse);
	glLightfv(GL_LIGHT0, GL_SPECULAR, light_specular);
	glLightfv(GL_LIGHT0, GL_POSITION, light_position);
	glEnable(GL_LIGHT0);

	glMaterialfv(GL_FRONT, GL_AMBIENT, mat_A);
	glMaterialfv(GL_FRONT, GL_DIFFUSE, mat_D);
	glMaterialfv(GL_FRONT, GL_SPECULAR, mat_S);
	glMaterialfv(GL_FRONT, GL_EMISSION, mat_E);
	glMaterialfv(GL_FRONT, GL_SHININESS, &mat_Shiny);
}

void desenhaObjectos3D() {

	// ... chamada das rotinas auxiliares de desenho ...
	glPushMatrix(); {
		//desenhaFundo();

		glPushName(1);
		desenhaPlanetaGenerico(mercurio);
		glPopName();
		glPushName(2);
		desenhaPlanetaGenerico(venus);
		glPopName();
		glPushName(3);
		desenhaPlanetaComLuas(terra, lua);
		glPopName();
		glPushName(4);
		desenhaPlanetaComAlien(marte);
		glPopName();
		glPushName(5);
		desenhaPlanetaGenerico(jupiter);
		glPopName();
		glPushName(6);
		desenhaPlanetaAnel(saturno, anel_saturno);
		glPopName();
		glPushName(7);
		desenhaPlanetaAnel(urano, anel_urano);
		glPopName();
		glPushName(8);
		desenhaPlanetaGenerico(neptuno);
		glPopName();
		glPushName(9);
		desenhaPlanetaGenerico(plutao);
		glPopName();
		glPushName(0);
		desenhaSol();//sol
		glPopName();
		desenhaUniverso(13, 400);//fundo

	}glPopMatrix();


}

void desenhaStopBtn(int x, int y, int size) {
	glColor3f(1.0, 1.0, 0.0);
	glBegin(GL_POLYGON);
	glVertex3f(x, y, 0.0);
	glVertex3f(x + size, y, 0.0);
	glVertex3f(x + size, y + size, 0.0);
	glVertex3f(x, y + size, 0.0);
	glEnd();
}

void desenhaPlayBtn(int x, int y, int size) {
	glColor3f(1.0, 1.0, 0.0);
	glBegin(GL_TRIANGLES);
	glVertex3f(x, y, 0.0);
	glVertex3f(x, y + size, 0.0);
	double dist = sqrt(pow(size, 2) - pow(size / 2.0, 2));
	glVertex3f(x + dist, y + (size / 2.0), 0.0);
	glEnd();
}

void desenhaMinusBtn(int x, int y, int comp, int larg) {

	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	glColor4f(0.0, 1.0, 0.0, 0.1);
	glBegin(GL_POLYGON);
	glVertex3f(x, y - (comp / 2) + 2, 0.0);
	glVertex3f(x, y + (comp / 2) + 2, 0.0);
	glVertex3f(x + comp, y + (comp / 2) + 2, 0.0);
	glVertex3f(x + comp, y - (comp / 2) + 2, 0.0);
	glEnd();
	glDisable(GL_BLEND);


	glColor3f(1.0, 1.0, 0.0);
	glBegin(GL_POLYGON);
	glVertex3f(x, y, 0.0);
	glVertex3f(x + comp, y, 0.0);
	glVertex3f(x + comp, y + larg, 0.0);
	glVertex3f(x, y + larg, 0.0);
	glEnd();
}

void desenhaPlusBtn(int x, int y, int comp, int larg) {

	desenhaMinusBtn(x, y, comp, larg);

	x = x + (comp / 2.0 - larg / 2.0);
	y = y - (comp / 2.0 - larg / 2.0);
	glColor3f(1.0, 1.0, 0.0);
	glBegin(GL_POLYGON);
	glVertex3f(x, y, 0.0);
	glVertex3f(x + larg, y, 0.0);
	glVertex3f(x + larg, y + comp, 0.0);
	glVertex3f(x, y + comp, 0.0);
	glEnd();
}

// design buttons rotate begin

void desenhaRightBtn(int x, int y, int size) {
	
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	glColor4f(0.0, 1.0, 0.0,0.1);
	glBegin(GL_POLYGON);
	glVertex3f(x, y, 0.0);
	glVertex3f(x, y + size, 0.0);
	glVertex3f(x + size, y + size, 0.0);
	glVertex3f(x + size, y, 0.0);
	glEnd();
	glDisable(GL_BLEND);

	glColor3f(1.0, 1.0, 0.0);

	glBegin(GL_TRIANGLES);
	glVertex3f(x, y, 0.0);
	glVertex3f(x, y + size, 0.0);
	double dist = sqrt(pow(size, 2) - pow(size / 2.0, 2));
	glVertex3f(x + dist, y + (size / 2.0), 0.0);
	glEnd();
}

void desenhaLeftBtn(int x, int y, int size) {

	double dist = sqrt(pow(size, 2) - pow(size / 2.0, 2));

	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	glColor4f(0.0, 1.0, 0.0, 0.1);
	glBegin(GL_POLYGON);
	glVertex3f(x - dist, y, 0.0);
	glVertex3f(x - dist, y + size, 0.0);
	glVertex3f(x , y + size, 0.0);
	glVertex3f(x , y, 0.0);
	glEnd();
	glDisable(GL_BLEND);

	glColor3f(1.0, 1.0, 0.0);
	glBegin(GL_TRIANGLES);
	glVertex3f(x - dist, y + (size / 2.0), 0.0);
	glVertex3f(x, y, 0.0);
	glVertex3f(x, y + size, 0.0);
	glEnd();
}

void desenhaUpBtn(int x, int y, int size) {

	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	glColor4f(0.0, 1.0, 0.0, 0.1);
	glBegin(GL_POLYGON);
	glVertex3f(x , y, 0.0);
	glVertex3f(x , y + size, 0.0);
	glVertex3f(x + size, y + size, 0.0);
	glVertex3f(x + size, y, 0.0);
	glEnd();
	glDisable(GL_BLEND);


	glColor3f(1.0, 1.0, 0.0);
	glBegin(GL_TRIANGLES);
	double dist = sqrt(pow(size, 2) - pow(size / 2.0, 2));
	glVertex3f(x  + (size / 2.0), y  , 0.0);
	glVertex3f(x , y + size, 0.0);
	glVertex3f(x + size , y + size , 0.0);
	glEnd();
}

void desenhaDownBtn(int x, int y, int size) {
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	glColor4f(0.0, 1.0, 0.0, 0.1);
	glBegin(GL_POLYGON);
	glVertex3f(x, y, 0.0);
	glVertex3f(x, y + size, 0.0);
	glVertex3f(x + size, y + size, 0.0);
	glVertex3f(x + size, y, 0.0);
	glEnd();
	glDisable(GL_BLEND);


	glColor3f(1.0, 1.0, 0.0);
	glBegin(GL_TRIANGLES);
	double dist = sqrt(pow(size, 2) - pow(size / 2.0, 2));
	glVertex3f(x + (size / 2.0), y + size, 0.0);
	glVertex3f(x, y, 0.0);
	glVertex3f(x + size, y , 0.0);
	glEnd();
}

// teste design buttons Rotate end

void setView2D(GLboolean Picking, int x, int y) {
	int vport[4];

	glMatrixMode(GL_PROJECTION);
	glPushMatrix();
	glLoadIdentity();
	if (Picking) { // se est� no modo picking, l� viewport e define zona de picking
		glGetIntegerv(GL_VIEWPORT, vport);
		gluPickMatrix(x, vport[3] - y, 4, 4, vport); // zona sobre o rato (+/-)
	}
	glOrtho(0.0, 950, 500, 0.0, -1.0, 10.0);
	glMatrixMode(GL_MODELVIEW);
}

void preparaCena2D() {

	glDisable(GL_DEPTH_TEST);
	glDisable(GL_CULL_FACE);
	glDisable(GL_TEXTURE_2D);
	glDisable(GL_LIGHTING);

	setView2D(GL_FALSE, 0, 0);
	glLoadIdentity();
}

void desenhaObjectos2D() {

	int x, y;
	glColor3f(1, 1, 1);
	bitmapCenterString(modelo.selecionado, 200, 20);
	bitmapCenterString("ROTACAO:", 850, 20);
	glPushName(111);
	if (estado.play)
		desenhaStopBtn(910, 6, 16);
	else
		desenhaPlayBtn(910, 6, 16);
	glPopName();

	x = 40, y = 490;
	glColor3f(1, 1, 1);
	bitmapCenterString("SOM:", x, y);
	glPushName(120);
	desenhaMinusBtn(x + 40, y - 10, 16, 4);
	glPopName();
	glPushName(130);
	desenhaPlusBtn(x + 60, y - 10, 16, 4);
	glPopName();

	glColor3f(1, 1, 1);
	x = 850, y = 490;
	bitmapCenterString("ZOOM:", x - strlen("ZOOM:") * 4, y);
	glPushName(140);
	desenhaMinusBtn(x + 40, y - 10, 16, 4);
	glPopName();
	glPushName(150);
	desenhaPlusBtn(x + 60, y - 10, 16, 4);
	glPopName();

	// teste draw buttons begin

	glPushName(160);
	desenhaRightBtn(45, 420,  16);
	glPopName();

	glPushName(170);
	desenhaLeftBtn(20, 420, 16);
	glPopName();

	glPushName(180);
	desenhaUpBtn(25, 400, 16);
	glPopName();

	glPushName(190);
	desenhaDownBtn(25, 440, 16);
	glPopName();


	// teste draw buttons end

	glColor3f(1, 1, 1);
	glMatrixMode(GL_PROJECTION);
	glPopMatrix();
	glMatrixMode(GL_MODELVIEW);
}

void Draw(void)
{
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glLoadIdentity();
	setCamera();

	preparaCena3D();
	desenhaObjectos3D();

	preparaCena2D();
	desenhaObjectos2D();

	glFlush();
	if (estado.doubleBuffer)
		glutSwapBuffers();
}

Planeta getSelected(int id) {
	switch (id) {
	case 1:
		return mercurio;
	case 2:
		return venus;
	case 3:
		return terra;
	case 4:
		return marte;
	case 5:
		return jupiter;
	case 6:
		return saturno;
	case 7:
		return urano;
	case 8:
		return neptuno;
	case 9:
		return plutao;
	}
}


/**
	Timer
*/
void Timer(int value)
{
	glutTimerFunc(estado.delayMovimento, Timer, 0);

	if (estado.play)
		 modelo.theta[0]+=0.8;

	if (estado.selected == 0) {
		estado.camera.center.x = 0;
		estado.camera.center.y = 0;
	}

	if (estado.selected >0) {

		Planeta p = getSelected(estado.selected);
		estado.camera.center.x = p.getCoordX();
		estado.camera.center.y = p.getCoordY();

		GLfloat ang = atan2(p.getCoordY(), p.getCoordX());

		estado.camera.eye.x = p.getCoordX() + (p.getRaio() + 40) * cos(ang ) * cos(RAD(controlo.angulo_y));
		estado.camera.eye.y = p.getCoordY() + (p.getRaio() + 40) * sin(ang  )* cos(RAD(controlo.angulo_y));
		estado.camera.eye.z = (p.getRaio() + 30) * sin(RAD(controlo.angulo_y));
		
	}else {
		estado.camera.eye.x = controlo.raio * cos(RAD(controlo.angulo_x)) * cos(RAD(controlo.angulo_y));
		estado.camera.eye.y = controlo.raio * sin(RAD(controlo.angulo_x)) * cos(RAD(controlo.angulo_y));
		estado.camera.eye.z = controlo.raio * sin(RAD(controlo.angulo_y));
	}

	glutPostRedisplay();
}

/**
	Callback para interaccao via teclado
*/
void Key(unsigned char key, int x, int y)
{
	switch (key) {

	case 27:
		exit(1); 
	case 'r':
		estado.play = !estado.play;
		break;
	case 'w':
		controlo.angulo_y++;
		break;
	case 's':
		controlo.angulo_y--;
		break;
	case 'd':
		controlo.angulo_x++;
		break;
	case 'a':
		controlo.angulo_x--;
		break;
	case 'q':
		if (controlo.raio < 400)
			controlo.raio++;
		break;
	case 'e':
		if (controlo.raio > RAIO_SOL)
			controlo.raio--;
		break;
	case 'm':
		if (!estado.SoundEngine->isCurrentlyPlaying(MAIN_TRACK))
			estado.SoundEngine->play2D(MAIN_TRACK, GL_TRUE);
		break;
	case 'n':
		estado.SoundEngine->stopAllSounds();
		break;
	case 'f':
	case 'F':
		glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
		break;
	case 'p':
	case 'P':
		glPolygonMode(GL_FRONT_AND_BACK, GL_POINT);
		break;
	case 'l':
	case 'L':
		glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
		break;
	}

	if (estado.debug)
		printf("Carregou na tecla %c\n", key);

}

void onPicking(int pick) {
	switch (pick) {
	case 111:
		estado.play = !estado.play;
		break;
	case 120:
		if (estado.sound > 0)
			estado.sound -= 0.1;
		estado.SoundEngine->setSoundVolume(estado.sound);
		break;
	case 130:
		if (estado.sound < 1)
			estado.sound += 0.1;
		estado.SoundEngine->setSoundVolume(estado.sound);
		break;
	case 150:
		if (controlo.raio > 0)
			controlo.raio-=5;
		break;
	case 140:
		if (controlo.raio < 400)
			controlo.raio += 5;
		break;
	case 170:
		controlo.angulo_x--;
		break;
	case 160:
		controlo.angulo_x++;
		break;
	case 180:
		controlo.angulo_y++;
		break;
	case 190:
		controlo.angulo_y--;
		break;

	case 0:
		strcpy(modelo.selecionado, "SOL");
		estado.selected = 0;
		break;
	case 1:
		strcpy(modelo.selecionado, "MERCURIO");
		estado.selected = 1;
		break;
	case 2:
		strcpy(modelo.selecionado, "VENUS");
		estado.selected = 2;
		break;
	case 3:
		strcpy(modelo.selecionado, "TERRA");
		estado.selected = 3;
		break;
	case 4:
		strcpy(modelo.selecionado, "MARTE");
		estado.selected = 4;
		break;
	case 5:
		strcpy(modelo.selecionado, "JUPITER");
		estado.selected = 5;
		break;
	case 6:
		strcpy(modelo.selecionado, "SATURNO");
		estado.selected = 6;
		break;
	case 7:
		strcpy(modelo.selecionado, "URANO");
		estado.selected = 7;
		break;
	case 8:
		strcpy(modelo.selecionado, "NEPTUNO");
		estado.selected = 8;
		break;
	case 9:
		strcpy(modelo.selecionado, "PLUTAO");
		estado.selected = 9;
		break;
	}
}

void endPicking(int x,int y) {
	// redefinic�o da vista (pois foi alterada pelo picking)
	setView(GL_FALSE, x, y);
	glutPostRedisplay();
}

#define BUFF_SIZE 100
void OnMouseClick(int bt, int st, int x, int y)
{
	int hits, i, k;
	GLuint   buffer[BUFF_SIZE], *bufp, numnames, name;
	GLdouble zmax, zmin;

	if (st != GLUT_DOWN)
		return;

	// inicio do picking

	glSelectBuffer(BUFF_SIZE, buffer);  // buffer onde coloca os dados do picking
	glRenderMode(GL_SELECT);            // inicializa o modo de picking
	glInitNames();                      // inicializa a pilha de nomes
	setView(GL_TRUE, x, y);             // define a vista para picking e a zona de picking
	glLoadIdentity();
	setCamera();
	desenhaObjectos3D();
	hits = glRenderMode(GL_RENDER);     // inicializa o modo de rendering(devolve o numero de hits do picking)

										// analise dos resultados do picking

	printf("3D**** %d Hits ****\n", hits);
	if (hits)
	{
		k = 0;
		bufp = buffer;
		for (i = 0; i < hits; i++)
		{
			// quantidade de nomes
			numnames = *bufp++;
			// profundidades da janela (serve para saber qual o objecto que esta 'a frente)
			zmin = (GLdouble)*bufp++ / UINT_MAX;  // cordenada z de janela minima do objecto entre 0 e 1
			zmax = (GLdouble)*bufp++ / UINT_MAX;  // cordenada z de janela maxima do objecto entre 0 e 1
			printf(" Hit %d, %d nomes - zmax=%.4f zmin=%.4f\n", i, numnames, zmax, zmin);

			// nomes que o objecto tem
			while (numnames--) {
				name = *bufp++;
				printf("  box: %u\n", name);
				onPicking(name);
				endPicking(x, y);
				return;
			}
		}
	}


	glSelectBuffer(BUFF_SIZE, buffer);  // buffer onde coloca os dados do picking
	glRenderMode(GL_SELECT);            // inicializa o modo de picking
	glInitNames();                      // inicializa a pilha de nomes

	setView2D(GL_TRUE, x, y);             // define a vista para picking e a zona de picking
	glLoadIdentity();
	//setCamera();
	desenhaObjectos2D();
	hits = glRenderMode(GL_RENDER);     // inicializa o modo de rendering(devolve o numero de hits do picking)

										// analise dos resultados do picking

	printf("2D**** %d Hits ****\n", hits);
	if (hits)
	{
		k = 0;
		bufp = buffer;
		for (i = 0; i < hits; i++)
		{
			// quantidade de nomes
			numnames = *bufp++;
			// profundidades da janela (serve para saber qual o objecto que esta 'a frente)
			zmin = (GLdouble)*bufp++ / UINT_MAX;  // cordenada z de janela minima do objecto entre 0 e 1
			zmax = (GLdouble)*bufp++ / UINT_MAX;  // cordenada z de janela maxima do objecto entre 0 e 1
			printf(" Hit %d, %d nomes - zmax=%.4f zmin=%.4f\n", i, numnames, zmax, zmin);

			// nomes que o objecto tem
			while (numnames--) {
				name = *bufp++;
				printf("  box: %u\n", name);
				onPicking(name);
				endPicking(x, y);
				return;
			}
		}
	}
	endPicking(x, y);
	return;
}

int main(int argc, char **argv)
{
	char str[] = " makefile MAKEFILE Makefile ";
	estado.doubleBuffer = 1;

	glutInit(&argc, argv);
	glutInitWindowPosition(0, 0);
	glutInitWindowSize(950, 500);
	glutInitDisplayMode(((estado.doubleBuffer) ? GLUT_DOUBLE : GLUT_SINGLE) | GLUT_RGB | GLUT_DEPTH);
	if (glutCreateWindow("Exemplo") == GL_FALSE)
		exit(1);

	Init();
	glutReshapeFunc(Reshape);
	glutDisplayFunc(Draw);

	glutKeyboardFunc(Key);
	glutMouseFunc(OnMouseClick);

	glutTimerFunc(estado.delayMovimento, Timer, 0);
	glutMainLoop();
	return 0;
}