#pragma once
class Planeta {
private:
	float p_distancia, p_rotacao_sol, p_raio, p_rotacao;
	int p_textura,p_coord_x,p_coord_y;
	char* p_nome;
public:
	Planeta(float distancia, float rotacao_sol, float raio, float rotacao, int textura,char* nome);

	float getDistancia() { return p_distancia; }
	float getRotacaoSol() { return p_rotacao_sol; }
	float getRaio() { return p_raio; }
	float getRotacao() { return p_rotacao; }
	int getTextura() { return p_textura; }
	char* getNome() { return p_nome; }
	int getCoordX() { return p_coord_x; }
	int getCoordY() { return p_coord_y; }
	void setCoords(int coord_x, int coord_y) {
		p_coord_x = coord_x;
		p_coord_y = coord_y;
	}
};

