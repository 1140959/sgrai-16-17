#pragma once

#define TEX_NUM 14

#define TEX_SOL 0
#define TEX_MERCURIO 1
#define TEX_VENUS 2
#define TEX_TERRA 3
#define TEX_LUA 4
#define TEX_MARTE 5
#define TEX_JUPITER 6
#define TEX_SATURNO 7
#define TEX_SATURNO_ANEL 8
#define TEX_URANO 9
#define TEX_URANO_ANEL 10
#define TEX_NEPTUNO 11
#define TEX_PLUTAO 12
#define TEX_UNIVERSO 13
